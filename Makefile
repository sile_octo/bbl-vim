.PHONY: bbl_1
bbl_1:
	vim -Nu vimrc premier_bbl.txt

.PHONY: bbl_2
bbl_2:
	vim -Nu vimrc second_bbl.txt

.PHONY: bbl_3
bbl_3:
	vim -Nu vimrc troisieme_bbl.txt
